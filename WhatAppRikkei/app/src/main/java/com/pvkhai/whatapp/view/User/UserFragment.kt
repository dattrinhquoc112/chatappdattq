package com.pvkhai.whatapp.view.User

import android.app.Activity
import android.content.Intent
import android.content.res.Configuration
import android.content.res.Resources
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelStore
import androidx.lifecycle.ViewModelStoreOwner
import androidx.navigation.findNavController
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.pvkhai.whatapp.R
import com.pvkhai.whatapp.databinding.FragmentLoginBinding
import com.pvkhai.whatapp.databinding.FragmentUserInfoBinding
import com.pvkhai.whatapp.service.FirebaseService
import com.pvkhai.whatapp.util.LanguageDialog
import com.pvkhai.whatapp.util.LoadingDialog
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_user.*
import kotlinx.android.synthetic.main.fragment_user.view.*
import kotlinx.android.synthetic.main.fragment_user_info.*
import kotlinx.android.synthetic.main.fragment_user_info.view.*
import java.util.*

class UserFragment : Fragment() {

    var firebase = FirebaseService();
    private var currentUser: FirebaseUser? = null;
    lateinit var mAuth: FirebaseAuth;
    lateinit var loadingDialog: LoadingDialog;
    lateinit var languageDialog: LanguageDialog

    val GALLARY_SELECT = 1;
    var configuration: Configuration? = null;
    var locale: Locale? = null;


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_user, container, false);
        retriveUserInfo()
        init()
        showlanguage(view)
        changelanguage(view)
        logout(view)

        view.usercircleImageView.setOnClickListener { v ->
        }
        view.coverImageView.setOnClickListener { v ->
            updateCover(v)
        }
        return view
    }

    private fun changelanguage(view: View?) {

        view?.language?.setOnClickListener { v ->
            languageDialog.startLanguageDialog()

        }
    }

    private fun showlanguage(view: View?) {
        if (Locale.getDefault().getDisplayLanguage().toString() == "Tiếng Việt") {
            view?.text_language?.text = "Tiếng Việt"
        }
        if (Locale.getDefault().getDisplayLanguage().toString() == "English") {
            view?.text_language?.text = "English"
        }
    }


    private fun logout(view: View) {

        view.ln_logout.setOnClickListener { v ->
            mAuth.signOut();
            requireActivity().viewModelStore.clear();
            v.findNavController().navigate(R.id.action_homeFragment_to_loginFragment)

        }
        view.img_edt.setOnClickListener { v ->

            v.findNavController().navigate(R.id.action_homeFragment_to_userInfoFragment)
        }
    }

    private fun init() {
        mAuth = FirebaseAuth.getInstance();
        currentUser = mAuth!!.currentUser
        loadingDialog = activity?.let { LoadingDialog(it) }!!;
        languageDialog = activity?.let { LanguageDialog(it) }!!;

    }


    // Get Information
    private fun retriveUserInfo() {
        firebase.rootRef.child("Users").child(firebase.getID())
            .addValueEventListener(object : ValueEventListener {
                override fun onCancelled(error: DatabaseError) {
                    TODO("Not yet implemented")
                }

                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.exists()) {
                        edt_fullname.text = snapshot.child("name").value.toString();
                        edt_email.text = currentUser?.email.toString()
                        if (usercircleImageView != null) Picasso.get().load(snapshot.child("image").value.toString()).into(usercircleImageView)
                        if (coverImageView != null )Picasso.get().load(snapshot.child("imageCover").value.toString()).into(coverImageView)
                    }
                    else {
                        if (usercircleImageView != null) Picasso.get()
                            .load(snapshot.child("image").value.toString())
                            .placeholder(R.mipmap.ic_placeholder)
                            .into(usercircleImageView)
                        if (coverImageView != null) Picasso.get()
                            .load(snapshot.child("imageCover").value.toString())
                            .into(coverImageView)
                    }
                }
            })
    }

    private fun updateCover(v: View) {
        var gallaryIntent: Intent = Intent();
        gallaryIntent.action = Intent.ACTION_GET_CONTENT;
        gallaryIntent.type = "image/*";
        startActivityForResult(gallaryIntent, GALLARY_SELECT);
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        loadingDialog.startLoadingDialog();
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GALLARY_SELECT) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                var imageUri: Uri = data.data!!;


                var filePath = firebase.userImageProfileRef.child(firebase.getID() + ".jpg")


                filePath.putFile(imageUri).addOnCompleteListener { task ->
                    if (task.isSuccessful) {

                        Toast.makeText(
                            activity,
                            R.string.upload_image_successfull,
                            Toast.LENGTH_LONG
                        )
                            .show();

                        filePath.downloadUrl.addOnSuccessListener { uri ->
                            var downloadUrl: Uri = uri
                            firebase.rootRef.child("Users").child(firebase.getID())
                                .child("imageCover")
                                .setValue(downloadUrl.toString())
                                .addOnCompleteListener { task ->
                                    if (task.isSuccessful) {
                                        Picasso.get().load(downloadUrl.toString())
                                            .into(coverImageView)
                                    } else {
                                    }
                                }
                        }

                    } else
                        Toast.makeText(activity, R.string.upload_image_fail, Toast.LENGTH_SHORT)
                            .show();
                    loadingDialog.dismissLoadingDialog();

                }
            } else {
                loadingDialog.dismissLoadingDialog();
            }
        }
    }
}