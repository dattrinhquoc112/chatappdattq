package com.pvkhai.whatapp

import android.app.Activity
import android.content.Context.MODE_PRIVATE
import android.content.res.Configuration
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.findNavController
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.database.FirebaseDatabase
import java.util.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        loadLocale()
    }

    fun loadLocale(){
        val sharePreferences = getSharedPreferences("Settings",Activity.MODE_PRIVATE)
        val language = sharePreferences.getString("App Language","")


        val locale = Locale(language)
        Locale.setDefault(locale)
        val config = Configuration()
        config.locale = locale
        resources.updateConfiguration(config,resources.displayMetrics)
    }


}