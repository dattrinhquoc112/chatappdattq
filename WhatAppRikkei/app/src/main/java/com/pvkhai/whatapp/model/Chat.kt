package com.pvkhai.whatapp.model

import android.R
import java.io.Serializable

class Chat : Serializable{
    var checkseen: Boolean= false;
    var form: String = "";
    var isShow: Boolean = false;
    var message: String = "";
    var time: String = "";
    var type: String = "";

    constructor(
        checkseen: Boolean,
        form: String,
        isShow: Boolean,
        message: String,
        time: String,
        type: String
    ) {
        this.checkseen = checkseen
        this.form = form
        this.isShow = isShow
        this.message = message
        this.time = time
        this.type = type
    }

    constructor()

    fun toMap(): Map<String, Any>? {
        val result: HashMap<String, Any> = HashMap()
        result["message"] = message;
        result["type"] = type;
        result["form"] = form
        result["time"] = time
        result["isshow"] = isShow;
        result["checkseen"] = checkseen;
        return result
    }

}