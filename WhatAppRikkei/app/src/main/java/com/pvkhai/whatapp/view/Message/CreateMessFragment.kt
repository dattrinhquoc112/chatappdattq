package com.pvkhai.whatapp.view.Message

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.pvkhai.whatapp.R
import com.pvkhai.whatapp.adapter.RecyclerFriendAdapter
import com.pvkhai.whatapp.adapter.RecyclerMessageAdapter
import kotlinx.android.synthetic.main.fragment_create_mess.view.*


class CreateMessFragment : Fragment() {

    lateinit var adapter: RecyclerFriendAdapter;

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_create_mess, container, false);
        val test = ArrayList<String>();
        test.add("1");
        test.add("1");
        test.add("1");
        test.add("1");
        adapter = RecyclerFriendAdapter(test)

        val layoutManager = LinearLayoutManager(context);
        view.rc_friend.layoutManager = layoutManager;
        view.rc_friend.addItemDecoration(
            DividerItemDecoration(
                view.rc_friend.context,
                layoutManager.orientation
            )
        )

        view.rc_friend.adapter = adapter;
        return view
    }


}