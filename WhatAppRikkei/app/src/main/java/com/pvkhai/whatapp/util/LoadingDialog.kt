package com.pvkhai.whatapp.util

import android.app.Activity
import android.app.AlertDialog
import android.view.LayoutInflater
import com.pvkhai.whatapp.R

class LoadingDialog (val activity: Activity) {
    var dialog: AlertDialog? = null;
    fun startLoadingDialog(){
        var builder: AlertDialog.Builder = AlertDialog.Builder(activity);
        val inflater: LayoutInflater = activity.layoutInflater;
        builder.setView(inflater.inflate(R.layout.custom_dialog,null));
        builder.setCancelable(false);


        dialog = builder.create();
        dialog?.show();
    }
    fun dismissLoadingDialog(){
        dialog?.dismiss();
    }
}