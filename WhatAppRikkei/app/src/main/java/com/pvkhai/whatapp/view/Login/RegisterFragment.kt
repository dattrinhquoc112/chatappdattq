package com.pvkhai.whatapp.view.Login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.navigation.NavOptions
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.pvkhai.whatapp.R
import com.pvkhai.whatapp.databinding.FragmentLoginBinding
import com.pvkhai.whatapp.databinding.FragmentRegisterBinding
import com.pvkhai.whatapp.model.User
import com.pvkhai.whatapp.service.FirebaseService
import com.pvkhai.whatapp.util.LoadingDialog
import com.pvkhai.whatapp.viewmodel.LoginViewModel
import com.pvkhai.whatapp.viewmodel.RegisterViewModel
import kotlinx.android.synthetic.main.fragment_register.*
import kotlinx.android.synthetic.main.fragment_register.toolbar
import kotlinx.android.synthetic.main.fragment_register.view.*
import kotlinx.android.synthetic.main.fragment_user_info.*

class RegisterFragment : Fragment() {

    var firebase = FirebaseService();
    private lateinit var mAuth: FirebaseAuth;
    lateinit var loading: LoadingDialog;
    lateinit var binding: FragmentRegisterBinding;
    lateinit var rootRef: DatabaseReference;
    val registerViewModel: RegisterViewModel by activityViewModels();

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false);

        val view = binding.root;


        init();

        binding.lifecycleOwner = viewLifecycleOwner
        binding.user = registerViewModel;

        handleToolBar(view);
        handleEvent(view);

        return view;
    }

    private fun handleEvent(view: View) {
        view.btn_register.setOnClickListener {
            createNewAccount()
        }

        view.tv_login.setOnClickListener {
            backLoginfragment();
        }
        view.edt_passwordregister.addTextChangedListener {
            view.btn_register.isEnabled = buttonActive()
        }
        view.edt_fullname.addTextChangedListener {
            view.btn_register.isEnabled = buttonActive()
        }
        view.edt_email.addTextChangedListener {
            view.btn_register.isEnabled = buttonActive()
        }
        view.cb.setOnCheckedChangeListener { compoundButton, b ->
            view.btn_register.isEnabled = buttonActive()
        }


    }

    private fun buttonActive(): Boolean {
        return !(edt_fullname.text.toString().equals("")
                || edt_email.text.toString().equals("")
                || edt_passwordregister.text.toString().equals("")
                || !cb.isChecked)
    }

    private fun init() {
        loading = activity?.let { LoadingDialog(it) }!!
        mAuth = FirebaseAuth.getInstance();
        rootRef = FirebaseDatabase.getInstance().getReference();

    }

    private fun createNewAccount() {


        loading.startLoadingDialog()

        mAuth.createUserWithEmailAndPassword(edt_email.text.toString(), edt_passwordregister.text.toString())

            .addOnCompleteListener { task ->
                if (task != null && task.isSuccessful) {
                    var userID = mAuth.currentUser?.uid;

//                    rootRef.child("Users").child(userID!!).setValue("")

                    Toast.makeText(context, R.string.register_successfull, Toast.LENGTH_LONG).show();
                    loading.dismissLoadingDialog();

                    allowUserLogin()

                } else {
                    Toast.makeText(context, R.string.register_fail, Toast.LENGTH_LONG).show();
                    loading.dismissLoadingDialog();
                }
            }
    }


    private fun handleToolBar(view: View) {
        if (activity is AppCompatActivity) {
            (activity as AppCompatActivity).setSupportActionBar(toolbar);
            view.toolbar.setNavigationIcon(R.mipmap.ic_back);
            view.toolbar.setNavigationOnClickListener {
                backLoginfragment()
            }
        }

    }

    fun backLoginfragment() {
        this.findNavController().navigateUp();
    }

    private fun allowUserLogin() {

        loading.startLoadingDialog()
        mAuth.fetchSignInMethodsForEmail(edt_email.text.toString()).addOnCompleteListener { task ->

            if (task.result?.signInMethods?.size != 0){

                mAuth.signInWithEmailAndPassword(edt_email.text.toString(), edt_passwordregister.text.toString())
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {

                            Toast.makeText(context, R.string.login_successfull, Toast.LENGTH_LONG).show();

                            loading.dismissLoadingDialog();

                            putUserInfo()

                            sendUserToHome()

                            edt_email.text = null
                            edt_passwordregister.text = null
                            edt_fullname.text = null


                        } else
                            Toast.makeText(context, R.string.login_successfull, Toast.LENGTH_LONG).show();
                        loading.dismissLoadingDialog();
                    }?.addOnCanceledListener {
                        Toast.makeText(context, R.string.login_failed, Toast.LENGTH_LONG).show();
                        loading.dismissLoadingDialog()
                    }

            }
            else {
                Toast.makeText(context, R.string.infor_notexisted, Toast.LENGTH_LONG).show();
                loading.dismissLoadingDialog()
            }

        }

    }

    private fun sendUserToHome() {
        this.findNavController().navigate(
            R.id.action_registerFragment_to_homeFragment, null,
            NavOptions.Builder()
                .setPopUpTo(
                    R.id.loginFragment,
                    true
                ).build()
        )
    }

    private fun putUserInfo() {
        var profile = HashMap<String, String>();
        profile.put("uid", firebase.getUser()?.uid!!)
        profile.put("name", edt_fullname.text.toString());
        profile.put("status", "1");
        profile.put("numberphone", "Phone");
        profile.put("dateofbirth", "02/09/1975");
        profile.put("image",context?.getString(R.string.default_image).toString())
        profile.put("imageCover",context?.getString(R.string.default_image).toString())
        firebase.rootRef.child("Users").child(firebase.getUser()?.uid!!).setValue(profile);
    }

}