package com.pvkhai.whatapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pvkhai.whatapp.model.UserInfo

class FriendViewModel:ViewModel() {
    var search: MutableLiveData<String> =  MutableLiveData();
    var listUser : MutableLiveData<ArrayList<UserInfo>> = MutableLiveData();
}