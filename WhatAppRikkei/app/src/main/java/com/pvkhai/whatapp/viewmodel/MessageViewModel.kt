package com.pvkhai.whatapp.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pvkhai.whatapp.model.UserInfo

class MessageViewModel :ViewModel(){
    var search: MutableLiveData<String> =  MutableLiveData();
    var listMessage : MutableLiveData<ArrayList<UserInfo>> =  MutableLiveData();

}